## react-native-chat-app

React-Native-chat-app is a Chat App. RN based frontend and using firebase.

## Screenshots
![image](./src/dating1.jpg)
![image](./src/dating2.jpg)
![image](./src/dating3.jpg)
![image](./src/dating4.jpg)

* ## Prerequisites
1. Globally installed React native  - `npm install -g react-native-cli`
2. Mac OS to build iOS app.

## Installation
This app is built with the React Native CLI. Once you have the [CLI installed](https://docs.reactnative.org/start/quick-setup), start by cloning the repo:
1. `git clone https://github.com/SumitRedsky/chat-app.git`
2. `cd chat-app`
3. `npm install` 

## App Features
1.Login with Facebook
2.Verify yourself and only hear from other verified users
3.Swipe through profiles with ease
4.Flirt with singles near you
5.Meet new people
6.Firebase based real-time chat and notifications
7.And Find love


## Run iOS Application
`react-native run-ios` 

## Run Android Application
`react-native run-android`


    
