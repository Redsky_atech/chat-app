import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ReactNative,
  TouchableHighlight,
  Text,
  StyleSheet,
  Dimensions,
  Image
} from "react-native";
import { View, Title, Screen } from "@shoutem/ui";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Messages from "../containers/Messages";
import Input from "../containers/Input";
import { sendMessage } from "../actions";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";
import Emoticons from "react-native-emoticons";
import renderIf from "../actions/renderIf";
import ImagePicker from "react-native-image-picker";
import { AsyncStorage } from "react-native";
import firebase from "../firebase";
import EmojiPicker from "react-native-smart-emoji-picker";
import fileType from "react-native-file-type";
import TrackPlayer from "react-native-track-player";

var width = Dimensions.get("window").width;
var height = Dimensions.get("window").height;

const mapStateToProps = state => ({
  chatHeight: state.chatroom.meta.height,
  user: state.user
});

const options = {
  title: "Select Image",
  // customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

// const CUSTOM_EMOJIS = {
//   categoryOne: [
//     {
//       code: "[a:1178]", // The key which your app server can recognize and map to an unique image.
//       image: "http://bbs.uestc.edu.cn/static/image/smiley/alu/65.gif" // Custom emoji url or local image path.
//     },
//     {
//       code: "[a:1179]",
//       image: "http://bbs.uestc.edu.cn/static/image/smiley/alu/66.gif"
//     }
//   ],
//   categoryTwo: [
//     {
//       code: "[s:763]",
//       image: "http://bbs.uestc.edu.cn/static/image/smiley/lu/01.gif"
//     }
//   ]
// };

class ChatUI extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollViewHeight: 0,
      inputHeight: 0,
      showEmoticons: false,
      showTextField: true,
      showKeyboard: false,
      showUploadEmoticon: true,
      imageFilename: "",
      spinner: false,
      imageSource: ""
    };
    console.disableYellowBox = true;
  }

  componentDidMount() {
    this.scrollToBottom(false);
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  _uploadEmoticonsPress() {
    this.setState({
      showEmoticons: true,
      showTextField: true,
      showKeyboard: true,
      showUploadEmoticon: false
    });
  }

  _keyboardPress() {
    this.setState({
      showEmoticons: false,
      showTextField: true,
      showKeyboard: false,
      showUploadEmoticon: true
    });
  }

  _onEmoticonPress = data => {
    let msg = {
      text: data.code,
      time: Date.now(),
      author: {
        name: this.props.user.name,
        avatar: this.props.user.avatar
      }
    };
    const newMsgRef = firebase
      .database()
      .ref("messages")
      .push();
    msg.id = newMsgRef.key;
    newMsgRef.set(msg);
    // return sendMessage(data.code, this.props.user);
  };

  _uploadAudioPress() {
    TrackPlayer.setupPlayer().then(async () => {
      // Adds a track to the queue
      await TrackPlayer.add({
        id: "trackId",
        url: require("https://jatt.download/music/data/Single_Track/201903/College/128/College_1.mp3"),
        title: "Track Title",
        artist: "Track Artist"
        // artwork: require("track.png")
      });
      // Starts playing it
      TrackPlayer.play();
    });
  }

  _uploadImagePress() {
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        alert("User cancelled image picker");
        // this.setState({ spinner: false });
      } else if (response.error) {
        alert("ImagePicker Error: ", response.error);
        // this.setState({ spinner: false });
      } else if (response.customButton) {
        alert("User tapped custom button: ", response.customButton);
      } else {
        this.setState({ imageFilename: response.fileName });
        const source = { uri: response.uri };
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          imageSource: source
        });
        var base64 = "data:image/jpeg;base64,";
        var imageData = base64.concat(response.data);
        console.log(imageData);
        // AsyncStorage.setItem("imageData", response.data);
        let msg = {
          text: imageData,
          time: Date.now(),
          author: {
            name: this.props.user.name,
            avatar: this.props.user.avatar
          }
        };
        const newMsgRef = firebase
          .database()
          .ref("messages")
          .push();
        msg.id = newMsgRef.key;
        newMsgRef.set(msg);
      }
    });
  }

  onScrollViewLayout = event => {
    const layout = event.nativeEvent.layout;

    this.setState({
      scrollViewHeight: layout.height
    });
  };

  onInputLayout = event => {
    const layout = event.nativeEvent.layout;

    this.setState({
      inputHeight: layout.height
    });
  };

  scrollToBottom(animate = true) {
    const { scrollViewHeight, inputHeight } = this.state,
      { chatHeight } = this.props;

    const scrollTo = chatHeight - scrollViewHeight + inputHeight;

    if (scrollTo > 0) {
      this.refs.scroll.scrollToPosition(0, scrollTo, animate);
    }
  }

  _scrollToInput(reactRef) {
    this.refs.scroll.scrollToFocusedInput(ReactNative.findNodeHandle(reactRef));
  }

  sendMessage = text => {
    return sendMessage(text, this.props.user);
  };

  render() {
    return (
      <Screen style={{ width: width, backgroundColor: "white" }}>
        <Title styleName="h-center" style={{ paddingTop: 20 }}>
          Global Chatroom
        </Title>
        <KeyboardAwareScrollView
          ref="scroll"
          onLayout={this.onScrollViewLayout}
        >
          <Messages />
        </KeyboardAwareScrollView>
        {renderIf(this.state.showTextField)(
          <View style={styles.inputContainer}>
            {renderIf(this.state.showUploadEmoticon)(
              <TouchableHighlight
                style={{
                  width: width * 0.15,
                  height: height * 0.08
                }}
                onPress={() => this._uploadEmoticonsPress()}
                underlayColor="#2196F3"
              >
                <Image
                  style={{
                    height: height * 0.04,
                    resizeMode: "contain",
                    width: width * 0.15,
                    marginTop: height * 0.02
                  }}
                  source={require("../assets/images/uploadEmoticons.png")}
                />
              </TouchableHighlight>
            )}
            {renderIf(this.state.showKeyboard)(
              <TouchableHighlight
                style={{
                  width: width * 0.15,
                  height: height * 0.08
                }}
                onPress={() => this._keyboardPress()}
                underlayColor="#2196F3"
              >
                <Image
                  style={{
                    height: height * 0.04,
                    resizeMode: "contain",
                    width: width * 0.15,
                    marginTop: height * 0.02
                  }}
                  source={require("../assets/images/keyboard.png")}
                />
              </TouchableHighlight>
            )}
            <Input
              onLayout={this.onInputLayout}
              //   onFocus={this._scrollToInput.bind(this)}
              submitAction={this.sendMessage}
              ref="input"
              placeholder="Say something cool ..."
            />
            <TouchableHighlight
              style={{
                height: height * 0.08,
                width: width * 0.1
              }}
              onPress={() => this._uploadAudioPress()}
              underlayColor="#2196F3"
            >
              <Image
                style={{
                  height: height * 0.04,
                  resizeMode: "contain",
                  width: width * 0.1,
                  marginTop: height * 0.02
                }}
                source={require("../assets/images/uploadAudio.png")}
              />
              {/* <Text>Upload</Text> */}
            </TouchableHighlight>
            <TouchableHighlight
              style={{
                height: height * 0.08,
                width: width * 0.15
              }}
              onPress={() => this._uploadImagePress()}
              underlayColor="#2196F3"
            >
              <Image
                style={{
                  height: height * 0.04,
                  resizeMode: "contain",
                  width: width * 0.15,
                  marginTop: height * 0.02
                }}
                source={require("../assets/images/uploadClip.png")}
              />
              {/* <Text>Upload</Text> */}
            </TouchableHighlight>
          </View>
        )}
        {renderIf(this.state.showEmoticons)(
          <View style={styles.emoticonsContainer}>
            <Emoticons
              onEmoticonPress={this._onEmoticonPress.bind(this)}
              //  onBackspacePress={this._onBackspacePress.bind(this)}
              //  show={this.state.showEmoticons}
              show={true}
              concise={false}
              showHistoryBar={true}
              showPlusBar={false}
            />
            {/* <EmojiPicker
              emojis={CUSTOM_EMOJIS}
              onEmojiPress={this._onEmoticonPress.bind(this)}
            /> */}
          </View>
        )}
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    height: height * 0.08,
    flexDirection: "row",
    width: width * 0.9,
    alignSelf: "center",
    marginBottom: 5,
    borderRadius: 30,
    backgroundColor: "#C9C9C9"
  },
  emoticonsContainer: {
    height: height * 0.5,
    flexDirection: "row",
    width: width,
    alignSelf: "center",
    backgroundColor: "white"
  }
});

export default connect(mapStateToProps)(ChatUI);
