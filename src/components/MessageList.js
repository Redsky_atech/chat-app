// Object.defineProperty(exports, "__esModule", { value: true });

import React, { Component } from "react";
import {
  ListView,
  Text,
  Row,
  Image,
  View,
  Subtitle,
  Caption,
  Heading
} from "@shoutem/ui";
import moment from "moment";
// import renderIf from "../actions/renderIf";
// import { AsyncStorage } from "react-native";

// var user_service_1 = require("../services/user.service");

// AsyncStorage.getItem("imageData", (err, result) => {
//   if (result != null) {
//     console.log(result);
//   }
// });

// showText: true;

const TextMessage = ({ msg }) => (
  <Row>
    <Image styleName="small-avatar top" source={{ uri: msg.author.avatar }} />
    <View styleName="vertical">
      <View styleName="horizontal space-between">
        <Subtitle>{msg.author.name}</Subtitle>
        <Caption>{moment(msg.time).from(Date.now())}</Caption>
      </View>
      <Text styleName="multiline">{msg.text}</Text>
    </View>
  </Row>
);
const ImageMessage = ({ msg }) => (
  <Row>
    <Image styleName="small-avatar top" source={{ uri: msg.author.avatar }} />
    <View styleName="vertical">
      <View styleName="horizontal space-between">
        <Subtitle>{msg.author.name}</Subtitle>
        <Caption>{moment(msg.time).from(Date.now())}</Caption>
      </View>
      <Image style={{ height: 100, width: 100 }} source={{ uri: msg.text }} />
    </View>
  </Row>
);

const TextMessageList = ({ messages, onLayout }) => (
  <ListView
    data={messages}
    autoHideHeader={true}
    renderRow={msg => <TextMessage msg={msg} />}
    onLayout={onLayout}
  />
);
const ImageMessageList = ({ messages, onLayout }) => (
  <ListView
    data={messages}
    autoHideHeader={true}
    renderRow={msg => <ImageMessage msg={msg} />}
    onLayout={onLayout}
  />
);
export { TextMessageList, ImageMessageList };

// export ImageMessageList;
// export default TextMessageList;
