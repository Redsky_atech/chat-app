import React, { Component } from "react";
import { connect } from "react-redux";

import { TextInput } from "@shoutem/ui";
import { Dimensions } from "react-native";

var width = Dimensions.get("window").width;
var height = Dimensions.get("window").height;

class Input extends Component {
  state = {
    text: null
  };

  onChangeText = text => this.setState({ text: text });

  onSubmitEditing = () => {
    this.props.dispatch(this.props.submitAction(this.state.text));
    if (!this.props.noclear) {
      this.setState({
        text: null
      });
    }
  };

  onFocus = event => {
    if (this.props.onFocus) {
      this.props.onFocus(this.refs.input);
    }
  };

  onBlur = () => {
    if (this.props.submitOnBlur) {
      this.onSubmitEditing();
    }
  };

  onLayout = event => {
    if (this.props.onLayout) {
      this.props.onLayout(event);
    }
  };

  render() {
    return (
      <TextInput
        style={{
          height: height * 0.08,
          width: width * 0.50,
          // placeholderTextColor: "black",
          backgroundColor: "#C9C9C9",
          padding: 0,
          color: "black"
          // backgroundColor: "red"
        }}
        placeholder={this.props.placeholder}
        onChangeText={this.onChangeText}
        onSubmitEditing={this.onSubmitEditing}
        onLayout={this.onLayout}
        value={this.state.text}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        ref="input"
      />
    );
  }
}

export default connect()(Input);
