import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Spinner } from "@shoutem/ui";

import { TextMessageList, ImageMessageList } from "../components/MessageList";
import { updateMessagesHeight } from "../actions";

const mapStateToProps = state => ({
  messages: state.chatroom.messages,
  isFetching: state.chatroom.meta.isFetching
});

const Messages = connect(mapStateToProps)(
  ({ messages, isFetching, dispatch }) => {
    // var messagesSize = messages.length;
    // var type = "";
    // for (i = 0; i < messagesSize; i++) {
    //   var msg = JSON.stringify(messages[i]);
    //   if (msg != undefined) {
    //     const messageData = JSON.parse(msg);
    //     console.log(messageData.text);
    //     var base64 = "data:image/jpeg;base64,";
    //     type = "";
    //     if (messageData.text.includes(base64)) {
    //       type = "image";
    //     }
    if (isFetching) {
      return (
        <View style={{ paddingTop: 50, paddingBottom: 50 }}>
          <Spinner />
        </View>
      );
    }
    // else if (type == "image") {
    //   return (
    //     <ImageMessageList
    //       messages={messages}
    //       style={{ minHeight: 100 }}
    //       onLayout={event => dispatch(updateMessagesHeight(event))}
    //     />
    //   );
    // }
    else {
      return (
        <TextMessageList
          messages={messages}
          style={{ minHeight: 100 }}
          onLayout={event => dispatch(updateMessagesHeight(event))}
        />
      );
    }
  }
  // }
  // return null;
  // }
);

export default Messages;
