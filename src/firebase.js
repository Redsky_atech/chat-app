import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBXKtLvRJlowilBbLuE_J59bgNgoGYytFc",
    authDomain: "chatappdemo-6fac7.firebaseapp.com",
    databaseURL: "https://chatappdemo-6fac7.firebaseio.com",
    projectId: "chatappdemo-6fac7",
    storageBucket: "chatappdemo-6fac7.appspot.com",
    messagingSenderId: "460034860003"
};
firebase.initializeApp(config);

export default firebase;