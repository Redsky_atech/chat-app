// "use strict";
// Object.defineProperty(exports, "__esModule", { value: true });
// import {
//   ListView,
//   Text,
//   Row,
//   Image,
//   View,
//   Subtitle,
//   Caption,
//   Heading
// } from "@shoutem/ui";
// import moment from "moment";
// var core_1 = require("@angular/core");
// var Subject_1 = require("rxjs/Subject");

// const ImageMessage = ({ msg }) => (
//   <Row>
//     <Image styleName="small-avatar top" source={{ uri: msg.author.avatar }} />
//     <View styleName="vertical">
//       <View styleName="horizontal space-between">
//         <Subtitle>{msg.author.name}</Subtitle>
//         <Caption>{moment(msg.time).from(Date.now())}</Caption>
//       </View>
//       <Image style={{ height: 100, width: 100 }} source={{ uri: msg.text }} />
//     </View>
//   </Row>
// );

// const TextMessageList = ({ messages, onLayout }) => (
//   <ListView
//     data={messages}
//     autoHideHeader={true}
//     renderRow={msg => <TextMessage msg={msg} />}
//     onLayout={onLayout}
//   />
// );

// const ImageMessageList = ({ messages, onLayout }) => (
//   <ListView
//     data={messages}
//     autoHideHeader={true}
//     renderRow={msg => <ImageMessage msg={msg} />}
//     onLayout={onLayout}
//   />
// );

// var UserService = /** @class */ (function() {
//   function UserService() {
//     this._showText = new Subject_1.Subject();
//     this.showTextData = this._showText.asObservable();
//     this._showImage = new Subject_1.Subject();
//     this.showTextData = this._showImage.asObservable();
//   }

//   UserService.prototype.showText = function(state) {
//     const TextMessage = ({ msg }) => (
//       <Row>
//         <Image
//           styleName="small-avatar top"
//           source={{ uri: msg.author.avatar }}
//         />
//         <View styleName="vertical">
//           <View styleName="horizontal space-between">
//             <Subtitle>{msg.author.name}</Subtitle>
//             <Caption>{moment(msg.time).from(Date.now())}</Caption>
//           </View>
//           <Text styleName="multiline">{msg.text}</Text>
//         </View>
//       </Row>
//     );

//     const TextMessageList = ({ messages, onLayout }) => (
//       <ListView
//         data={messages}
//         autoHideHeader={true}
//         renderRow={msg => <TextMessage msg={msg} />}
//         onLayout={onLayout}
//       />
//     );

//     this._showText.next(TextMessageList);
//   };

//   UserService.prototype.showImage = function(state) {
//     this._showImage.next(state);
//   };

//   UserService = __decorate(
//     [core_1.Injectable(), __metadata("design:paramtypes", [])],
//     UserService
//   );
//   return UserService;
// })();
// exports.UserService = UserService;
